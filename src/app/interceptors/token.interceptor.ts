import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor
} from '@angular/common/http';
import { LoginService } from '../services/login.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router'
import { HttpErrorResponse } from '@angular/common/http';
@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    constructor(public auth: LoginService,private router: Router) { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        request = request.clone({
            setHeaders: {
                Authorization: `Bearer ${this.auth.getToken()}`
            }
        }), (err:any) => {
            console.log(err)
            if (err instanceof HttpErrorResponse) {
                if (err.status === 401) {
                    this.router.navigate(['/back/login']);
                }
              }
        };
         return next.handle(request); 
    }
}