import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginHeaderComponent } from '../back-end/login-header/login-header.component';
import { SideBarComponent } from '../back-end/side-bar/side-bar.component';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from 'src/app/app-routing.module';



@NgModule({
  declarations: [
    LoginHeaderComponent,
    SideBarComponent,
  ],
  exports:[
    LoginHeaderComponent,
    SideBarComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    BrowserModule,
    AppRoutingModule,
  ]
})
export class SharedModule { }
