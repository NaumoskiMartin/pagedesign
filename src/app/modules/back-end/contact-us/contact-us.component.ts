import { Component, OnInit } from '@angular/core';
import { UseExistingWebDriver } from 'protractor/built/driverProviders';
import { AdminServiceService } from '../../../services/admin-service.service'
import { Router } from '@angular/router'
import { ToastrService } from 'ngx-toastr';
import { ContactService } from '../../../services/contact.service'
@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {

  contactData: any;
  allContactData:any = [];
  searchTerm:any = '';

  constructor(
    private adminService: AdminServiceService,
    private router: Router,
    private toastr: ToastrService,
    private contactService: ContactService
    ) { }

  ngOnInit(): void {
    this.contactService.getContacts().subscribe(data => {
      this.contactData = data["Contact-Us"];
      this.allContactData = this.contactData;
    })
  }
  viewDetails(id: any) {
    this.router.navigate(['/back/contact' + '/' + id]);
  }
  deleteContact(id: any) {
    this.contactService.deleteContact(id).subscribe(data => {
      this.toastr.success('You deleted contact!', 'Success!')
    });
  }
  filterContacts() {
    let tempTableData: any = [];
    this.allContactData.forEach((element:any) => {
      if (element.subject.toLowerCase().includes(this.searchTerm.toLowerCase())
        || element.name.toLowerCase().includes(this.searchTerm.toLowerCase())
        || element.email.toLowerCase().includes(this.searchTerm.toLowerCase())
        || element.created_at.toLowerCase().includes(this.searchTerm.toLowerCase())){
        tempTableData.push(element);
      }
    });
    this.searchTerm.length == 1 ? this.contactData = this.allContactData : this.contactData = tempTableData;
  }
 
}
