import { Component, OnInit } from '@angular/core';
import { AdminServiceService } from '../../../services/admin-service.service';
import { ActivatedRoute } from '@angular/router'
import { Subscription } from 'rxjs';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ArticlesService } from '../../../services/articles.service';

@Component({
  selector: 'app-edit-article',
  templateUrl: './edit-article.component.html',
  styleUrls: ['./edit-article.component.css']
})
export class EditArticleComponent implements OnInit {
  private routeSub!: Subscription;
  selectedFile: File;
  id = 0;
  article: any = [];
  formData: FormGroup;

  constructor(
    private articlesService: ArticlesService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private toastr: ToastrService
  ) { }
  ngOnInit(): void {
    this.takeArticleId();
  }
  onFileChanged(event: any) {
    this.selectedFile = event.target.files[0];
    this.formData.get('filenames[]') != null ? this.formData.get('filenames[]')?.setValue(this.selectedFile) : null;
  }
  updateArticle(form: any) {
    const formData = new FormData();
    formData.append('title', this.formData.get('title')?.value);
    formData.append('body', this.formData.get('body')?.value);
    formData.append('price', this.formData.get('price')?.value);
    formData.append('address', this.formData.get('address')?.value)
    formData.append('city', this.formData.get('city')?.value)
    formData.append('for', this.formData.get('for')?.value)
    formData.append('phone_number', this.formData.get('phone_number')?.value)
    formData.append('filenames[]', this.selectedFile);
    formData.append('type', this.formData.get('type')?.value)
    formData.append('available', this.formData.get('available')?.value);

    this.articlesService.editArticle(formData, this.id).subscribe(data => {
      this.toastr.info(data.Message)
    }, error => {
      this.toastr.error(error.error.message, 'Error');
    });
  }
  takeArticleId() {
    this.routeSub = this.route.params.subscribe(params => {
      this.id = params['id'];
    });
    this.articlesService.getArticle(this.id).subscribe(data => {
      this.article = data["Article"];
      this.formData = new FormGroup({
        title: new FormControl(this.article.title, Validators.required),
        body: new FormControl(this.article.body, Validators.required),
        price: new FormControl(this.article.price, Validators.required),
        address: new FormControl(this.article.address, Validators.required),
        city: new FormControl(this.article.city, Validators.required),
        for: new FormControl(this.article.for, Validators.required),
        phone_number: new FormControl(this.article.phonenumber, Validators.required),
        type: new FormControl(this.article.type, Validators.required),
        available: new FormControl(this.article.available, Validators.required),
      })
    }, error => {
    })
  }

  onSubmit(form: FormGroup) {
    if (form.valid) {
      this.updateArticle(form.value)
    }
    else {
      this.toastr.error('Fill all of the fields!', 'Error');
    }
  }

}
