import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from '../../guards/auth.guard';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { ContatDetailsComponent } from './contat-details/contat-details.component';
import { AriclesComponent } from './aricles/aricles.component';
import { DetailArticleComponent } from './detail-article/detail-article.component';
import { EditArticleComponent } from './edit-article/edit-article.component';
import { CreateArticleComponent } from './create-article/create-article.component';
import { AboutUsComponent } from './about-us/about-us.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'register',
    component: RegisterComponent,
  },
  {
    path:'reset-password',
    component: ResetPasswordComponent
  },
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'contact-us',
    component: ContactUsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'contact/:id',
    component: ContatDetailsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'articles',
    component: AriclesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'property/:id',
    component: DetailArticleComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'edit-article/:id',
    component: EditArticleComponent,
  },
  {
    path: 'create-article',
    component: CreateArticleComponent,
  },
  {
    path: 'about-us',
    component: AboutUsComponent,
    canActivate: [AuthGuard]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BackRoutingModule { }
