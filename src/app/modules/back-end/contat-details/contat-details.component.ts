import { Component, OnInit } from '@angular/core';
import { AdminServiceService } from '../../../services/admin-service.service'
import { ActivatedRoute } from '@angular/router'
import { Subscription } from 'rxjs';
import { ContactService } from '../../../services/contact.service'

@Component({
  selector: 'app-contat-details',
  templateUrl: './contat-details.component.html',
  styleUrls: ['./contat-details.component.css']
})
export class ContatDetailsComponent implements OnInit {
  contactData:any;
  private routeSub!: Subscription; 
  id = 0;
  constructor(private route: ActivatedRoute, private contactService: ContactService) { }

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      this.id = params['id'];
    });
    this.contactService.getDetailContact(this.id).subscribe(data => {
      this.contactData = data.Contact;
    }, error => {
    })
  }

}
