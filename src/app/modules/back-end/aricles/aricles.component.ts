import { Component, OnInit } from '@angular/core';
import { AdminServiceService } from '../../../services/admin-service.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ArticlesService } from '../../../services/articles.service';

@Component({
  selector: 'app-aricles',
  templateUrl: './aricles.component.html',
  styleUrls: ['./aricles.component.css']
})
export class AriclesComponent implements OnInit {

  tableData: any = [];
  searchTerm = '';
  allTableData: any = [];

  constructor(
    private articlesService: ArticlesService,
    private router: Router,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    this.getArticles();
  }

  getArticles() {
    this.articlesService.getArticles().subscribe(data => {
      this.tableData = data.Articles;
      this.allTableData = this.tableData;
    }, error => {
    })
  }
  filterARticles() {
    let tempTableData: any = [];
    this.allTableData.forEach((element: any) => {
      if (element.title.toLowerCase().includes(this.searchTerm.toLocaleLowerCase())
        || element.city.toLowerCase().includes(this.searchTerm.toLocaleLowerCase())
        || element.type.toLowerCase().includes(this.searchTerm.toLocaleLowerCase())
        || element.phonenumber.toLowerCase().includes(this.searchTerm.toLowerCase())
        || element.address.toLowerCase().includes(this.searchTerm.toLocaleLowerCase())) {
        tempTableData.push(element);
      }
    });
    this.searchTerm.length == 1 ? this.tableData = this.allTableData : this.tableData = tempTableData;
  }
  viewDetails(id: any) {
    this.router.navigate(['/back/property' + '/' + id]);
    window.open('/back/property/' + id, "_blank")
  }
  deleteArticle(id: any) {
    this.toastr.success('You deleted article!', 'Success!'); 
    this.articlesService.deleteArticle(id).subscribe(data => {
      this.getArticles();
    });
  }
  editArticle(id: any) {
    this.router.navigate(['/back/edit-article/' + id]);
  }
}
