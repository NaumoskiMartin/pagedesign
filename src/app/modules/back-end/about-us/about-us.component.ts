import { Component, OnInit } from '@angular/core';
import { AdminServiceService } from '../../../services/admin-service.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.css']
})
export class AboutUsComponent implements OnInit {
  aboutUsData: any = [];
  title = '';
  body = {
    title: '',
    body: ''
  };
  constructor(private mainService: AdminServiceService, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.getAbousUsData()
  }
  getAbousUsData() {
    this.mainService.getAboutUsData().subscribe((data) => {
      this.aboutUsData = data;
      this.body.title = this.aboutUsData["About us"].title;
      this.body.body = this.aboutUsData["About us"].body;
    });
  }
  updateAboutUs() {
    if (this.body.title == '' || this.body.body == '') {
      this.toastr.warning('Please fill all the fields');
    }
    else {
      this.mainService.updateAboutUs(this.body).subscribe(data => {
        data.Message == "You didnt change anything." ? this.toastr.warning(data.Message) : this.toastr.success(data.Message, 'Success!');
      }, err => { });
    }

  }

}
