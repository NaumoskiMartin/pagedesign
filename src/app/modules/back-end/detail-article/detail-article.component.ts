import { Component, OnInit } from '@angular/core';
import { AdminServiceService } from '../../../services/admin-service.service'
import { ActivatedRoute } from '@angular/router'
import { Subscription } from 'rxjs';
import { Router } from '@angular/router'
import { ArticlesService } from '../../../services/articles.service';

@Component({
  selector: 'app-detail-article',
  templateUrl: './detail-article.component.html',
  styleUrls: ['./detail-article.component.css']
})
export class DetailArticleComponent implements OnInit {
  articleData: any = [];
  private routeSub!: Subscription;
  id = 0;
  constructor(private articlesService: ArticlesService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.takeArticleDetails();
  }

  takeArticleDetails() {
    this.routeSub = this.route.params.subscribe(params => {
      this.id = params['id'];
    });
    this.articlesService.getDetailArticle(this.id).subscribe(data => {
      this.articleData = data.Article;
    }, error => {
    })
  }
  editArticle() {
    this.router.navigate(['/back/edit-article/' + this.id]);
  }

  showArticle() {
    this.router.navigate(['/front/detail-property/' + this.id])
  }
  ngOnDestroy() {
    this.routeSub.unsubscribe();
  }
}
