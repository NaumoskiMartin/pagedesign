import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BackRoutingModule } from './back-routing.module';
import { LoginComponent } from './login/login.component';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AriclesComponent } from './aricles/aricles.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { RegisterComponent } from '../back-end/register/register.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { HomeComponent } from './home/home.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { ContatDetailsComponent } from './contat-details/contat-details.component';
import { DetailArticleComponent } from './detail-article/detail-article.component';
import { EditArticleComponent } from './edit-article/edit-article.component';
import { CreateArticleComponent } from './create-article/create-article.component';
import { AboutUsComponent } from './about-us/about-us.component';


@NgModule({
  declarations: [
    LoginComponent,
    AriclesComponent,
    RegisterComponent,
    ResetPasswordComponent,
    HomeComponent,
    ContactUsComponent,
    ContatDetailsComponent,
    DetailArticleComponent,
    EditArticleComponent,
    CreateArticleComponent,
    AboutUsComponent
  ],
  entryComponents:[
    LoginComponent,
    AriclesComponent,
    RegisterComponent,
    ResetPasswordComponent,
    HomeComponent,
    ContactUsComponent,
    ContatDetailsComponent,
    DetailArticleComponent,
    EditArticleComponent,
    CreateArticleComponent,
    AboutUsComponent
  ],
  imports: [
    CommonModule,
    BackRoutingModule,
    SharedModule,
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
  ]
})
export class BackModule { }
