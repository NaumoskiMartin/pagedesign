import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminServiceService } from '../../../services/admin-service.service';

@Component({
  selector: 'app-login-header',
  templateUrl: './login-header.component.html',
  styleUrls: ['./login-header.component.css']
})
export class LoginHeaderComponent implements OnInit {
  @Output() notifyParent: EventEmitter<any> = new EventEmitter();

  constructor(private router: Router, private adminService: AdminServiceService) { }
  routeFlag: boolean = false;
  adminPanelFlag: boolean = false;
  ngOnInit(): void {
    let href = this.router.url;
    href == '/front' || href.includes("detail-property")  || href == "/front/about" || href == "/front/contact" || href == "/back/login"? this.routeFlag = true : this.routeFlag = false;
    this.adminPanelFlag = this.adminService.adminHeader();
  }
  toogleSideBar() {
    let sideBar = document.getElementById("mySidebar");
    let adminPanel = document.getElementById("adminPanel");
    if (sideBar != null && adminPanel != null) {
      if (sideBar.classList.contains("animation-in")) {
        sideBar.classList.add("animation-out");
        sideBar.classList.remove("animation-in");
        adminPanel.style.display = "none";
      }
      else {
        sideBar.classList.add("animation-in");
        sideBar.classList.remove("animation-out");
        adminPanel.style.display = "block";
      }
    }
  }
  logOut() {
    localStorage.removeItem('token');
    // localStorage.removeItem('email');
    // window.location.reload();
    this.router.navigate(['/back/login']);
  }

}
