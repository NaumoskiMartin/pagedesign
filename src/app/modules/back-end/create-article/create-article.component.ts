import { Component, OnInit } from '@angular/core';
import { AdminServiceService } from '../../../services/admin-service.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Router } from '@angular/router'
import { ToastrService } from 'ngx-toastr';
import { FormControl } from '@angular/forms';
import { ArticlesService } from '../../../services/articles.service';

@Component({
  selector: 'app-create-article',
  templateUrl: './create-article.component.html',
  styleUrls: ['./create-article.component.css']
})
export class CreateArticleComponent implements OnInit {
  selectedFile: File;
  formData: FormGroup;
  error: any = '';
  errFlag: boolean = false;
  constructor(
    private formBuilder: FormBuilder,
    private articlesService: ArticlesService,
    private router: Router,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    this.formData = new FormGroup({
      title: new FormControl('', Validators.required),
      body: new FormControl('', Validators.required),
      price: new FormControl('', Validators.required),
      address: new FormControl('', Validators.required),
      city: new FormControl('', Validators.required),
      for: new FormControl('', Validators.required),
      phone_number: new FormControl('', Validators.required),
      type: new FormControl('', Validators.required),
      available: new FormControl('', Validators.required),
    })
  }
  onFileChanged(event: any) {
    this.selectedFile = event.target.files[0];
    this.formData.get('filenames[]') != null ? this.formData.get('filenames[]')?.setValue(this.selectedFile) : null;
  }

  createArticle(form: any) {
    const formData = new FormData();
    formData.append('title', this.formData.get('title')?.value);
    formData.append('body', this.formData.get('body')?.value);
    formData.append('price', this.formData.get('price')?.value);
    formData.append('address', this.formData.get('address')?.value)
    formData.append('city', this.formData.get('city')?.value)
    formData.append('for', this.formData.get('for')?.value)
    formData.append('phone_number', this.formData.get('phone_number')?.value)
    formData.append('filenames[]', this.selectedFile);
    formData.append('type', this.formData.get('type')?.value)
    formData.append('available', this.formData.get('available')?.value);
    this.articlesService.createArticle(formData).subscribe(data => {
      this.router.navigate(['/back/articles']);
    }, error => {

    })
  }

  onSubmit(form: FormGroup) {
    if (form.valid) {
      this.createArticle(form.value)
    }
    else {
      this.toastr.error('Fill all of the fields!', 'Error');
    }
  }

}
