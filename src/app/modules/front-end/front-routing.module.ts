import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactComponent } from './contact/contact.component';
import { AboutComponent } from './about/about.component'
import { DataContentComponent } from './data-content/data-content.component';
import { DetailPropertyComponent } from './detail-property/detail-property.component';

const routes: Routes = [
  {
    path:'contact',
    component:ContactComponent
  },
  {
    path:'about',
    component:AboutComponent
  },
  {
    path: '',
    component: DataContentComponent,
  },
  {
    path:'detail-property/:id',
    component:DetailPropertyComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FrontRoutingModule { }
