import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FrontRoutingModule } from './front-routing.module';
import { HeaderComponent } from './header/header.component';
import { CarouselComponent } from './carousel/carousel.component'
import { AboutComponent } from './about/about.component';
import { FooterComponent } from './footer/footer.component';
import { SharedModule } from '../shared/shared.module';

 
@NgModule({
  declarations: [
    HeaderComponent,
    CarouselComponent,
    AboutComponent,
    FooterComponent,
  ],
  entryComponents:[
    HeaderComponent,
    CarouselComponent,
    AboutComponent,
    FooterComponent,
  ],
  exports:[
    HeaderComponent,
    CarouselComponent,
    AboutComponent,
    FooterComponent,
  ],
  imports: [
    CommonModule,
    FrontRoutingModule,
    SharedModule
  ]
})
export class FrontModule { }
