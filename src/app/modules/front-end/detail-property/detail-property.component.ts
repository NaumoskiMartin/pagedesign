import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AdminServiceService } from '../../../services/admin-service.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router'
import { environment } from '../../../../environments/environment';
import { ArticlesService } from '../../../services/articles.service';

@Component({
  selector: 'app-detail-property',
  templateUrl: './detail-property.component.html',
  styleUrls: ['./detail-property.component.css']
})
export class DetailPropertyComponent implements OnInit {
  private routeSub!: Subscription;
  id = 0;
  article:any = [];
  relatedArticles:any = [];
  pictureUrl:any = '';
  constructor(private articlesService: ArticlesService,private route: ActivatedRoute,private router: Router) { }
  ngOnInit(): void {
    this.takeArticleId();
    this.takeRelatedArticles();
  }
  takeRelatedArticles(){
    this.articlesService.getArticle(this.id).subscribe(data => {
      this.relatedArticles = data["Related-Article"];
    }, error => {
    })
  }
  showDetailArticle(id: any) {
    this.router.navigate(['/front/detail-property/' + id]);
    this.takeArticleId();
    this.takeRelatedArticles();
  }
  takeArticleId(){
    this.routeSub = this.route.params.subscribe(params => {
      this.id = params['id'];
    }); 
    this.articlesService.getArticle(this.id).subscribe(data => {
      this.article = data.Article;
      if(this.article.photo[0]){
        this.pictureUrl = environment.pictureUrl + this.article.photo[0].photo;
      }
      else{
        this.pictureUrl = "../../assets/hero_how_it_works_meal_plans.jpg"
      }
      //this.pictureUrl = environment.pictureUrl + this.article.photo[0].photo;
    }, error => {
    })
  }
}
