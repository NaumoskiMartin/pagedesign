import { Component, OnInit } from '@angular/core';
import { AdminServiceService } from '../../../services/admin-service.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
  aboutUsData: any = [];
  body = {
    title: '',
    body: ''
  };
  constructor(private mainService: AdminServiceService) { }

  ngOnInit(): void {
    this.getAbousUsData()
  }
  getAbousUsData() {
    this.mainService.getAboutUsData().subscribe((data) => {
      this.aboutUsData = data;
      this.body.title = this.aboutUsData["About us"].title;
      this.body.body = this.aboutUsData["About us"].body;
    });
  }
}
