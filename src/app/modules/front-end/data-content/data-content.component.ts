import { query } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { DataContentService } from '../../../services/data-content.service';
import { ArticlesService } from '../../../services/articles.service';
@Component({
  selector: 'app-data-content',
  templateUrl: './data-content.component.html',
  styleUrls: ['./data-content.component.css']
})
export class DataContentComponent implements OnInit {

  searchQuery: any = {
    price_form: 0,
    price_to: 2000,
    offer_types: '',
    city: '',
    type: '',
    page: 1
  };
  rowFlag: boolean = false;
  cardFlag: boolean = true;
  articles: any = [];
  searchData: any = [];
  tempFlag: boolean = false;
  numbers: any = [];
  constructor(private router: Router,
    private dataContentService: DataContentService,
    private artclesSercice: ArticlesService) { }
  ngOnInit(): void {
    this.takeArticles(1);
  }
  takeArticles(page: number) {
    this.searchQuery.page = page;
    this.dataContentService.search(this.searchQuery).subscribe(data => {
      this.articles = data.Articles.data;
      console.log(this.articles)
      this.countArticles(data.Articles.last_page);
    }, error => {
    });
  }
  countArticles(last_page: any) {
    if (this.numbers.length == 0) {
      for (let i = 1; i <= last_page; i++) {
        this.numbers.push(i)
      }
    }
  }
  showDetailArticle(id: any) {
    this.router.navigate(['/front/detail-property/' + id])
  }
  toogleShowFlags() {
    this.cardFlag = !this.cardFlag;
    this.rowFlag = !this.rowFlag;
  }
  makeSearchQuery() {
    this.dataContentService.search(this.searchQuery).subscribe(data => {
      this.searchData = data.Articles.data;
      this.articles = this.searchData;
      this.searchData ? this.tempFlag = true : false;
    }, error => {
    });
  }

}
