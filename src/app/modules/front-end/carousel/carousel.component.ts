import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router'
import { environment } from '../../../../environments/environment';
import { ArticlesService } from '../../../services/articles.service';
import { DataContentService } from '../../../services/data-content.service';
import { Router } from '@angular/router'

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css'],
})
export class CarouselComponent implements OnInit {
  articleData: any = [];
  private routeSub!: Subscription;
  id = 0;
  pictureUrl: any = undefined;
  randomArticle: any;
  opacityFlag: boolean = false;
  randomArticlePictureUrl: any = undefined;
  searchQuery: any = {
    price_form: 0,
    price_to: 2000,
    offer_types: '',
    city: '',
    type: '',
    page: 1
  };
  constructor(private route: ActivatedRoute, private articlesService: ArticlesService, private dataContentService: DataContentService, private router: Router,) {
  }
  ngOnInit(): void {
    this.takeId();
    this.pictureUrl != undefined ? '' : this.pictureUrl = "../../assets/hero_how_it_works_meal_plans.jpg"; this.opacityFlag = true;
  }
  ngOnChanges() {
    this.takeId();
    this.pictureUrl != undefined ? '' : this.pictureUrl = "../../assets/hero_how_it_works_meal_plans.jpg"; this.opacityFlag = true;
  }
  takeId() {
    this.routeSub = this.route.params.subscribe(params => {
      this.id = params['id'];
      this.id != undefined ? this.takeArticle() : this.takeRandom();
    });
    this.id != undefined ? this.takeArticle() : '';

  }
  showDetailArticle(id: any) {
    this.router.navigate(['/front/detail-property/' + id])
  }
  takeRandom() {
    this.dataContentService.search(this.searchQuery).subscribe(data => {
      if (data.Articles.data.length > 1) {
        let random = Math.floor(Math.random() * data.Articles.data.length);
        this.randomArticle = data.Articles.data[random];
        console.log("RANDOM:")
        console.log(this.randomArticle)
        this.randomArticlePictureUrl = this.randomArticle.photo.length > 0 && this.randomArticle.photo[0].photo ? environment.pictureUrl + this.randomArticle.photo[0].photo : '../../assets/hero_how_it_works_meal_plans.jpg';
        console.log(this.randomArticlePictureUrl)
      }
    })
  }
  takeArticle() {
    this.articlesService.getDetailArticle(this.id).subscribe(data => {
      this.articleData = data.Article;
      console.log("Data:" + this.articleData)

      if (this.articleData.photo[0]) {
        this.pictureUrl = environment.pictureUrl + this.articleData.photo[0].photo;
        this.opacityFlag = false;
      }
      else {
        this.pictureUrl = "../../assets/hero_how_it_works_meal_plans.jpg"
      }
    }, error => {
    })
  }


}
