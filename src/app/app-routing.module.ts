import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
const routes: Routes = [

  {
    path: '',
    redirectTo: '/front',
    pathMatch: 'full'
  },
  {
    path:'front',
    loadChildren: () => import('./modules/front-end/front-routing.module').then(m => m.FrontRoutingModule)
  },
  {
    path:'back',
    loadChildren: () => import('./modules/back-end/back-routing.module').then(m => m.BackRoutingModule)
  },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
