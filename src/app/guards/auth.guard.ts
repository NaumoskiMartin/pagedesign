import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ArticlesService } from '../services/articles.service';
import { LoginService } from '../services/login.service';
import { HttpClientModule } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private articlesService: ArticlesService,
    private loginService: LoginService,
    private http: HttpClient) { }

  // canActivate(): boolean {
  //   // this.canActivateTest();
  //   if (localStorage.getItem('token')) {
  //     return true;
  //   }
  //   else {
  //     this.router.navigate(['/back/login']);
  //     return false;
  //   }

  // }

  canActivate(): Promise<boolean> {
    return new Promise((resolve) => {
      this.articlesService.getArticles()
        .subscribe((data: any) => {
          resolve(true);
        },err => {
          this.router.navigate(['/back/login']);
          resolve(true);
        })
     
      })
    }

}
