import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class DataContentService {
  constructor(private http: HttpClient) { }
  search(searchQuery: any): Observable<any> {
    let price_fromm = searchQuery.price_from == undefined ? 0 : searchQuery.price_from;
    // searchQuery.type != '' ? searchQuery.type = searchQuery.type[0] + "%2B" + searchQuery.type[2] : '';
    return this.http.get(environment.baseUrl + '/api/search', {
      params: {
        'price_from': price_fromm,
        'price_to': searchQuery.price_to,
        'offer_types': searchQuery.offer_types,
        'city': searchQuery.city,
        'type': searchQuery.type,
        'page': searchQuery.page
      }
    })

  }
}
