import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpClient, HttpEvent, HttpHeaders, HttpRequest } from '@angular/common/http';
import { LoginService } from './login.service';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ArticlesService {

  constructor(private router: Router, private http: HttpClient, private auth: LoginService) { }

  getArticles(): Observable<any> {
    return this.http.get(environment.baseUrl + '/api/articles');
  }

  getDetailArticle(id: any): Observable<any> {
    return this.http.get(environment.baseUrl + '/api/article/property/' + id)
  }
  
  createArticle(body: any,): Observable<any> {
    return this.http.post(environment.baseUrl + '/api/create-article', body, {
      reportProgress: true,
      observe: 'events'
    })
  }
  deleteArticle(id: any): Observable<any> {
    return this.http.delete(environment.baseUrl + '/api/article/delete/' + id)
  }
  getArticle(id: any): Observable<any> {
    return this.http.get(environment.baseUrl + '/api/article/property/' + id)
  }
  editArticle(body: FormData, id: any): Observable<any> {
    const httpHeaders: HttpHeaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    return this.http.put<any>(environment.baseUrl + '/update-article/' + id, body,{headers:httpHeaders});
  }
}
