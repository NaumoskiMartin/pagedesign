import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class LoginService {


  constructor(private http:HttpClient) { }

  login(user:any,password:any): Observable<any> {
    return this.http.post(environment.baseUrl + '/api/auth/login',{email:user,password});
  }
  getToken(){
    return localStorage.getItem('token');
  }
  registerUser(user:any){
    return this.http.post(environment.baseUrl + '/api/auth/register',user)
  }
}
