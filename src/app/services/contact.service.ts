import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpClient, HttpEvent, HttpHeaders, HttpRequest } from '@angular/common/http';
import { LoginService } from './login.service';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ContactService {

  constructor(private router: Router, private http: HttpClient, private auth: LoginService) { }
  getContacts(): Observable<any> {
    return this.http.get(environment.baseUrl + '/api/contact-us')
  }
  getDetailContact(id: any): Observable<any> {
    return this.http.get(environment.baseUrl + '/api/contact/' + id)
  }
  deleteContact(id: any): Observable<any> {
    return this.http.delete(environment.baseUrl + '/api/contact/delete/' + id)
  }
}
