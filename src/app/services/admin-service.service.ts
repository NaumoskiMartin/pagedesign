import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpClient, HttpEvent, HttpHeaders, HttpRequest } from '@angular/common/http';
import { LoginService } from './login.service';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class AdminServiceService {

  adminPanelFlag: boolean = false;
  constructor(private router: Router, private http: HttpClient, private auth: LoginService) { }
  adminHeader() {
    let href = this.router.url;
    href == "/back/login" || href == "/back/register" || href == "/back/reset-password" ? this.adminPanelFlag = true : this.adminPanelFlag = false;
    return this.adminPanelFlag;
  }
  getAboutUsData(): Observable<any> {
    return this.http.get(environment.baseUrl + '/api/aboutus');
  }
  updateAboutUs(body: any): Observable<any> {
    return this.http.put<any>(environment.baseUrl + '/api/update/about-us', body);
  }
}
